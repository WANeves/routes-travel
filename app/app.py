from flask import Flask, request

from common.main import Main
from pathlib import Path


def set_input_file(enviroment: str):
    global DEFAULT_FILE_CSV
    input_file = "input-file-test.csv" if enviroment == 'test' else 'input-file.csv'
    _path = Path.cwd()
    if _path.name == 'routes-travel':
        DEFAULT_FILE_CSV = input_file
    else:
        parent = _path.parent.parent if enviroment == 'test' else _path.parent
        DEFAULT_FILE_CSV = Path(parent, input_file)


def create_app(enviroment='development'):
    app = Flask(__name__)
    app.config["DEBUG"] = True
    app.config['FLASK_ENV'] = enviroment
    set_input_file(enviroment)

    def make_main():
        main = Main(DEFAULT_FILE_CSV)
        main.validate_file()
        main.handle_input_data()
        main.make_graph()

        return main

    try:
        main = make_main()
    except:
        print(" ERROR: An Unexpected Error Occurred with GRAPH")

    @app.route('/', methods=['GET'])
    def home():
        return '<h1>Routes Travel API</h1><p>This site is a prototype API for Routes Travel.</p>'

    @app.route('/api/v1/travel/routes', methods=['GET', 'POST'])
    def best_route():
        origin = request.args.get('origin')
        destination = request.args.get('destination')
        price = request.args.get('price')

        if origin is None or destination is None:
            return 'Error: No origin/destination field provided. Please specify.', 400

        if request.method == 'GET':
            if not main.exists_point(origin, destination):
                return 'Error: Route not exists.', 400
            else:
                return main.get_route(origin, destination), 200
        else:
            if price is None:
                return 'Error: No price field provided. Please specify price.', 400
            resp = main.add_route(origin, destination, price)
            status_code = 201 if len(resp) > 1 else 400
            return resp, status_code

    return app


# Call the application factory function to construct a Flask application
# instance using the development configuration
app = create_app()


if __name__ == '__main__':
    app.run(port=8080)
