# Rota de Viagem #

Um turista deseja viajar pelo mundo pagando o menor preço possível independentemente do número de conexões necessárias.
Vamos construir um programa que facilite ao nosso turista, escolher a melhor rota para sua viagem.

Para isso precisamos inserir as rotas através de um arquivo de entrada.

## Input Example ##
```csv
GRU,BRC,10
BRC,SCL,5
GRU,CDG,75
GRU,SCL,20
GRU,ORL,56
ORL,CDG,5
SCL,ORL,20
```

## Explicando ## 
Caso desejemos viajar de **GRU** para **CDG** existem as seguintes rotas:

1. GRU - BRC - SCL - ORL - CDG ao custo de **$40**
2. GRU - ORL - CGD ao custo de **$64**
3. GRU - CDG ao custo de **$75**
4. GRU - SCL - ORL - CDG ao custo de **$45**

O melhor preço é da rota **1** logo, o output da consulta deve ser **GRU - BRC - SCL - ORL - CDG**.

### Execução do programa ###
A inicializacao do teste se dará por linha de comando onde o primeiro argumento é o arquivo com a lista de rotas inicial.

```shell
$ mysolution input-routes.csv
```

Duas interfaces de consulta devem ser implementadas:
- Interface de console deverá receber um input com a rota no formato "DE-PARA" e imprimir a melhor rota e seu respectivo valor.
  Exemplo:
  
```shell
please enter the route: GRU-CGD
best route: GRU - BRC - SCL - ORL - CDG > $40
please enter the route: BRC-CDG
best route: BRC - ORL > $30
```

- Interface Rest
    A interface Rest deverá suportar:
    - Registro de novas rotas. Essas novas rotas devem ser persistidas no arquivo csv utilizado como input(input-routes.csv),
    - Consulta de melhor rota entre dois pontos.

Também será necessária a implementação de 2 endpoints Rest, um para registro de rotas e outro para consula de melhor rota.

## Recomendações ##
Para uma melhor fluides da nossa conversa, atente-se aos seguintes pontos:

* Envie apenas o código fonte,
* Estruture sua aplicação seguindo as boas práticas de desenvolvimento,
* Evite o uso de frameworks ou bibliotecas externas à linguagem. Utilize apenas o que for necessário para a exposição do serviço,
* Implemente testes unitários seguindo as boas praticas de mercado,
* Documentação
  Em um arquivo Texto ou Markdown descreva:
  * Como executar a aplicação,
  * Estrutura dos arquivos/pacotes,
  * Explique as decisões de design adotadas para a solução,
  * Descreva sua APÌ Rest de forma simplificada.


## Overview

Esta aplicação possui as funcionalidades para pesquisar a melhor rota para uma viagem e também realizar a inclusão de novas rotas.


## Installation Instructions

Pull down the source code from this GitLab repository:

```shell
git clone https://gitlab.com/WANeves/routes-travel.git
```

Create a new **virtual environment**:

```shell
$ cd routes-travel
$ python3 -m venv venv
```
or
```shell
$ python -m venv venv
```

Activate the **virtual environment**:

```shell
$ source venv/bin/activate
```
[**windows**]
```shell
$ . venv/Scripts/activate
```


Install the python packages in requirements.txt:

```shell
(venv) $ pip install -r requirements.txt
```

Set the file that contains the Flask application and specify that the development environment should be used:

```shell
(venv) $ export FLASK_APP=app/app.py
(venv) $ export FLASK_RUN_PORT=8080
```

Run development server to serve the Flask application:

```shell
(venv) $ flask run
```

## Running Tests

```shell
(venv) $ pytest -v
```
![](/images/pytest_all.png)

## Python Modules Used

- Flask: micro-framework for web application development
- Pytest - framework for tests

This application is written using Python 3.9

## Running Console

To run console.py on the terminal
```shell
(venv) $ python console.py
```

enter the file or press enter to default
```shell
(venv) $ Enter filename csv (case None default is input-file.csv):
```

select one of the options
```shell
        - MENU OPTIONS -  
        
        1 - Find route
        2 - Add new route
        0 - Exit

Select an option : 1
```

inform the route to find the best price
```shell
(venv) $ please enter the route: Origin-Destination: BRC-CDG
best route: BRC - SCL - ORL - CDG > $30
```

add new route
```shell
(venv) $ Enter the new route in the format: Origin,Destination,Price
MFG,CDG,35
Route successfully added
```

## Applied language

Para o desafio proposto foi aplicada a utilização da linguagem Python para implementação de uma solução rápida e eficiente, 
permitindo o uso de Flask que é um framework que possui pouca dependência e complexidade para que uma API REST seja implementada, 
e pensando em testes de unidade e aceitação aplicamos o Pytest.

# Algorithm of Dijkstra
Para o problema do cenário de rota de viagem foi realizado um estudo para definir qual algoritmo de Grafos seria
melhor aplicado para o problema. Este cenário se assemelha a um conhecido problema, o caminho mínimo ou caxeiro 
viajante. E partindo deste princípio um dos algoritmos que resolve este problema é o Dijkstra, que aplica uma eurística para 
uma distância máxima entre quaisquer duas vertices 

![Dijkstra](https://i.ytimg.com/vi/pVfj6mxhdMw/hqdefault.jpg)

"O algoritmo considera um conjunto S de menores caminhos, iniciado com um vértice inicial I. A cada passo do algoritmo 
busca-se nas adjacências dos vértices pertencentes a S aquele vértice com menor distância relativa a I e adiciona-o a S e, então, repetindo os passos até que todos os vértices alcançáveis por I estejam em S. Arestas que ligam vértices já pertencentes a S são desconsideradas."  [Referencia](https://pt.wikipedia.org/wiki/Algoritmo_de_Dijkstra)

# Structure of files/packages

```shell
| routes-travel - Aplicação
| routes-travel | console.py - CLI - Interface de linha de comando
| routes-travel | input-file.csv - Arquivo de rotas
| routes-travel | input-file-test.csv - Arquivo de teste de rotas
| routes-travel | requirements.txt - Arquivo com packages para instalação 
| routes-travel | common | file.py - Pacote de manipulação do arquivo
| routes-travel | common | main.py - Pacote de manipulação do Grafo
| routes-travel | resources | model | graph.py - Dijkstra - Pacote do algoritmo 
| routes-travel | resources | model | priority_queue.py - Pacote manipulação de collections
| routes-travel | app | app.py - Arquivo Flask de manipulação da API 
| routes-travel | test | conftest.py - Configuração de testes
| routes-travel | test | test_graph.py - Pacote de testes do algoritmo
| routes-travel | test | test_main.py - Pacote de testes de manipulação do Grafo
| routes-travel | test | priority_queue.py - Pacote de testes de manipulação de collections
| routes-travel | test | app | conftest.py - Configuração de testes
| routes-travel | test | app | test_app.py - Pacotes de testes da API
```

# REST interface documentation

#### Add new route

- **URI:** /api/v1/travel/routes?origin=origin&destination=destination&price=price
- **METHOD:** POST
- **PARAMS**:
   - **origin**: Origin of the travel
       - Required
       - string
   - **destination**: Destination of the travel
       - Required
       - string
   - **price**: Price of the travel
       - Required
       - int
- **REPONSE:** 201
- **Payload:**
```json
{
    "destination": "CDG",
    "origin": "CFC",
    "price": 25
}
```

#### Search for the best flight route
- **URI:** /api/v1/travel/routes?origin=origin&destination=destination
- **METHOD:** GET
- **PARAMS**:
   - **origin**: Origin of the travel
       - Required
       - string
   - **destination**: Destination of the travel
       - Required
       - string
- **REPONSE:**
 
```json
{
   "bestRoute": [
       "GRU",
       "BRC",
       "SCL",
       "ORL",
       "CDG"
   ],
   "price": 40
}
```
