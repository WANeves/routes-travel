from app.app import create_app


def test_home_page():
    flask_app = create_app('test')

    # Create a test client using the Flask application configured for testing
    with flask_app.test_client() as test_client:
        response = test_client.get('/')
        assert response.status_code == 200
        assert b'Routes Travel API' in response.data
        assert b'This site is a prototype API for Routes Travel.' in response.data


def test_home_page_with_fixture(test_client):
    response = test_client.get('/')
    assert response.status_code == 200
    assert b'Routes Travel API' in response.data
    assert b'This site is a prototype API for Routes Travel.' in response.data


def test_get_best_route_gru_cdg(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/v1/travel/routes' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get('/api/v1/travel/routes', query_string={'origin': 'GRU', 'destination': 'CDG'})
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['bestRoute'] == ['GRU', 'BRC', 'SCL', 'ORL', 'CDG']
    assert response.json['price'] == 40


def test_get_best_route_brc_cdg(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/v1/travel/routes' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get('/api/v1/travel/routes', query_string={'origin': 'BRC', 'destination': 'CDG'})
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['bestRoute'] == ['BRC', 'SCL', 'ORL', 'CDG']
    assert response.json['price'] == 30


def test_get_route_not_exists(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/v1/travel/routes' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get('/api/v1/travel/routes', query_string={'origin': 'BRU', 'destination': 'CDG'})
    assert response.status_code == 400
    assert b'Error: Route not exists.' in response.data


def test_get_best_route_without_origin(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/v1/travel/routes' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get('/api/v1/travel/routes', query_string={'destination': 'CDG'})
    assert response.status_code == 400
    assert b'Error: No origin/destination field provided. Please specify.' in response.data


def test_get_best_route_without_destination(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/v1/travel/routes' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get('/api/v1/travel/routes', query_string={'origin': 'BRC'})
    assert response.status_code == 400
    assert b'Error: No origin/destination field provided. Please specify.' in response.data


def test_get_best_route_without_origin_and_destination(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/v1/travel/routes' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get('/api/v1/travel/routes')
    assert response.status_code == 400
    assert b'Error: No origin/destination field provided. Please specify.' in response.data


def test_success_add_route(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/v1/travel/routes' page is posted to (POST)
    THEN check the response is valid
    """
    response = test_client.post('/api/v1/travel/routes',
                                query_string={'origin': 'SLC', 'destination': 'CDG', 'price': 30},
                                follow_redirects=True)
    assert response.status_code == 201
    assert response.json is not None
    assert response.json['origin'] == 'SLC'
    assert response.json['destination'] == 'CDG'
    assert response.json['price'] == 30


def test_error_add_route_without_origin(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/v1/travel/routes' page is posted to (POST)
    THEN check the response is valid
    """
    response = test_client.post('/api/v1/travel/routes',
                                query_string={'destination': 'CDG', 'price': 30},
                                follow_redirects=True)
    assert response.status_code == 400
    assert b'Error: No origin/destination field provided. Please specify.' in response.data


def test_error_add_route_without_destination(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/v1/travel/routes' page is posted to (POST)
    THEN check the response is valid
    """
    response = test_client.post('/api/v1/travel/routes',
                                query_string={'origin': 'SLC', 'price': 30},
                                follow_redirects=True)
    assert response.status_code == 400
    assert b'Error: No origin/destination field provided. Please specify.' in response.data


def test_error_add_route_without_price(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/api/v1/travel/routes' page is posted to (POST)
    THEN check the response is valid
    """
    response = test_client.post('/api/v1/travel/routes',
                                query_string={'origin': 'SLC', 'destination': 'CDG'},
                                follow_redirects=True)
    assert response.status_code == 400
    assert b'Error: No price field provided. Please specify price.' in response.data
