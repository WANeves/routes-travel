import pytest
from pathlib import Path
from common.main import Main

INPUT_FILE = "input-file-test.csv"


@pytest.fixture(scope='module')
def default_input_file():
    parent = Path.cwd().parent

    if parent.name == 'routes-travel':
        input_file = Path(parent, INPUT_FILE)
    elif parent.name == 'test':
        input_file = Path(parent.parent, INPUT_FILE)
    else:
        input_file = INPUT_FILE

    return input_file


@pytest.fixture(scope='module')
def main_test(default_input_file):
    _main = Main(default_input_file)
    return _main


@pytest.fixture(scope='module')
def routes():
    return [['GRU', 'BRC', 10],
            ['BRC', 'SCL', 5],
            ['GRU', 'CDG', 75],
            ['GRU', 'SCL', 20],
            ['GRU', 'ORL', 56],
            ['ORL', 'CDG', 5],
            ['SCL', 'ORL', 20]]


def set_data_file(main, routes):
    for route in routes:
        main.add_route(route[0], route[1], route[2], False)


@pytest.fixture(scope='module')
def main(main_test, routes):
    main_test.file.clear_file()
    set_data_file(main_test, routes)
    main_test.handle_input_data()
    main_test.make_graph()
    return main_test
