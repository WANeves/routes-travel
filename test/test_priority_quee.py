import pytest

from resources.model.priority_quee import PriorityQueue


@pytest.fixture()
def priority_queue() -> PriorityQueue:
    return PriorityQueue()


def test_check_empty_queue(priority_queue):
    assert priority_queue.is_empty() is True


def test_check_enqueue(priority_queue):
    priority_queue.enqueue(['CDG', 0])
    assert priority_queue.is_empty() is False


def test_check_dequeue(priority_queue):
    priority_queue.enqueue(['CDG', 0])
    priority_queue.dequeue()

    assert priority_queue.is_empty() is True

