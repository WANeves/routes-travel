from common.main import Main

INVALID_INPUT_FILE = '../inputfile.csv'


def test_valid_file(main):
    resp = main.validate_file()
    assert resp is True


def test_invalid_file():
    _main = Main(INVALID_INPUT_FILE)
    try:
        _main.validate_file()
    except:
        assert True


def test_make_graph(default_input_file):
    _main = Main(default_input_file)

    assert len(_main.graph.points) == 0
    assert len(_main.graph.adjacents_points) == 0

    _main.handle_input_data()
    _main.make_graph()

    assert len(_main.graph.points) > 0
    assert len(_main.graph.adjacents_points) > 0


def test_get_route(main):
    resp = main.get_route('BRC', 'CDG')

    assert resp is not None
    assert resp['bestRoute'] == ['BRC', 'SCL', 'ORL', 'CDG']
    assert resp['price'] == 30


def test_exists_route(main):
    resp = main.exists_route('BRC', 'SCL')
    assert resp is True


def test_not_exists_route(main):
    resp = main.exists_route('BRC', 'SCE')
    assert resp is False


def test_add_route(main):
    resp = main.exists_route('BRC', 'ORL')
    assert resp is False
    main.add_route('BRC', 'ORL', 30)
    resp = main.exists_route('BRC', 'ORL')
    assert resp is True


def test_get_best_route_gru_cdg(main):
    origin, destination = 'GRU', 'CDG'
    resp = main.get_route(origin, destination)

    assert resp is not None
    assert resp['bestRoute'] == ['GRU', 'BRC', 'SCL', 'ORL', 'CDG']
    assert resp['price'] == 40


def test_get_best_route_brc_cdg(main):
    origin, destination = 'BRC', 'CDG'
    resp = main.get_route(origin, destination)

    assert resp is not None
    assert resp['bestRoute'] == ['BRC', 'SCL', 'ORL', 'CDG']
    assert resp['price'] == 30
