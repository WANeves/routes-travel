import pytest

from common.main import Main


def set_data_file(main, routes):
    for route in routes:
        main.add_route(route[0], route[1], route[2], False)


@pytest.fixture(scope='module')
def main_reset(default_input_file):
    _main = Main(default_input_file)
    return _main


def test_add_starting_points(main_reset, routes):
    _main = main_reset
    _main.file.clear_file()

    assert len(_main.graph.points) == 0

    set_data_file(_main, routes)
    _main.handle_input_data()
    _main.make_graph()

    assert len(_main.graph.points) > 0


def test_add_destination_points(main_reset, routes, default_input_file):
    _main = main_reset
    _main.file.clear_file()
    _main = Main(default_input_file)

    assert len(_main.graph.adjacents_points) == 0

    set_data_file(_main, routes)
    _main.handle_input_data()
    _main.make_graph()

    assert len(_main.graph.adjacents_points) > 0


def test_find_best_price(main):
    origin, destination = 'BRC', 'CDG'
    resp = main.get_route(origin, destination)

    assert resp is not None
    assert resp['bestRoute'] == ['BRC', 'SCL', 'ORL', 'CDG']
    assert resp['price'] == 30

