class PriorityQueue:

    def __init__(self):
        self._collection = []

    @property
    def collection(self):
        return self._collection

    def enqueue(self, element):
        if self.is_empty():
            self._collection.append(element)
        else:
            added = False
            for i in self._collection:
                if element[1] < i[1]:
                    self._collection.insert(0, element)
                    added = True
                    break

            if not added:
                self._collection.append(element)

    def dequeue(self) -> list:
        # VALIDAR SE REALMENTE É RETORNADO O VALOR
        value = self._collection.pop(0)
        return value

    def is_empty(self):
        return len(self.collection) == 0
