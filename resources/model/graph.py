from resources.model.priority_quee import PriorityQueue
from decimal import Decimal

POSITIVE_INFINITY = Decimal('Infinity')


class Graph:

    def __init__(self):
        self._points = []
        self._adjacents_points = {}

    @property
    def points(self):
        return self._points

    @points.setter
    def points(self, points: list):
        self._points = points

    @property
    def adjacents_points(self):
        return self._adjacents_points

    @adjacents_points.setter
    def adjacents_points(self, adjacents_points: list):
        self._adjacents_points = adjacents_points

    def __add_starting_points(self, point: str, is_add: bool):
        self.points.append(point)
        if not is_add:
            self.adjacents_points[point] = []
        else:
            if point not in self.adjacents_points:
                self.adjacents_points[point] = []

    def __add_destination_point(self, point1: str, point2: str, price: int):
        self.adjacents_points[point1].append({'point': point2, 'price': price})
        self.adjacents_points[point2].append({'point': point1, 'price': price})

    def insert_point_and_destination(self, input_data: list, is_add=False):
        self.__insert_starting_points(input_data, is_add)
        self.__insert_destination_point(input_data)

    def __insert_starting_points(self, input_data: list, is_add: bool):
        for values in input_data:
            self.__add_starting_points(values[0], is_add)
            self.__add_starting_points(values[1], is_add)

    def __insert_destination_point(self, input_data: list):
        for values in input_data:
            self.__add_destination_point(values[0], values[1], int(values[2]))

    def find_best_price(self, start_node: str, end_node: str):
        times = {}
        back_trace = {}

        priority = PriorityQueue()

        times[start_node] = 0

        for point in self.points:
            if point != start_node:
                times[point] = POSITIVE_INFINITY

        priority.enqueue([start_node, 0])

        while not (priority.is_empty()):
            shortest_step = priority.dequeue()
            current_node = shortest_step[0]

            for i in self.adjacents_points[current_node]:
                time = times[current_node] + i['price']

                if time < times[i['point']]:
                    times[i['point']] = time
                    back_trace[i['point']] = current_node
                    priority.enqueue([i['point'], time])

        path = [end_node]
        last_step = end_node

        while last_step != start_node:
            path = ([back_trace[last_step]] + path)
            last_step = back_trace[last_step]

        return {'bestRoute': path, 'price': times[end_node]}

    def exists_point(self, point: str) -> bool:
        return point in self.points

