from common.main import *

DEFAULT_FILE_CSV = 'input-file.csv'


def menu():

    return print('''
        - MENU OPTIONS -  
        
        1 - Find route
        2 - Add new route
        0 - Exit
        ''')


def add_route(main: Main):
    print('Enter the new route in the format: Origin,Destination,Price')

    origin, destination, price = input().split(',')
    success = len(main.add_route(origin, destination, int(price))) > 1

    if success:
        print('Route successfully added')
    else:
        print('Route exist or there was an error updating the file')


def get_travel(main: Main):
    print('please enter the route: Origin-Destination: ')

    origin, destination = input().split('-')
    try:
        if not main.exists_point(origin, destination):
            raise IOError
        else:
            resp = main.get_route(origin, destination)
            print_best_route(resp)
    except IOError:
        print("\n Error: Route not exists.")


def print_best_route(best_route):
    list = ' - '.join(best_route['bestRoute'])
    price = ''.join([' > $', str(best_route['price'])])
    print('best route:', ''.join([list, price]))


def make_main(input_file: str):
    main = Main(input_file)
    main.handle_input_data()
    main.make_graph()
    return main


if __name__ == '__main__':
    input_file = input(f"Enter filename csv (case None default is {DEFAULT_FILE_CSV}): ") or 'input-file.csv'

    main = make_main(input_file)

    option = 999
    while option != 0:
        menu()
        option = int(input('Select an option : '))

        if option == 1:
            get_travel(main)
        elif option == 2:
            add_route(main)
        elif option == 0:
            print('Good Bye')
        else :
            print('Invalid Option')


