from common.file import *
from resources.model.graph import Graph


class Main(object):

    def __init__(self, input_file=None):
        self._input_file = input_file
        self._file = File(self._input_file)
        self._graph = Graph()

    @property
    def file(self):
        return self._file

    @property
    def graph(self):
        return self._graph

    def validate_file(self):
        try:
            file = open(self._input_file, "r")
            if len(file.readline()) == 0:
                raise IOError
            else:
                return True
        except IOError:
            print("\n ERROR: Invalid or corrupted input files")
            sys.exit(0)

    def make_graph(self):
        self._graph.insert_point_and_destination(self._file.input_data)

    def handle_input_data(self):
        return self._file.read_entry()

    def add_route(self, origin: str, destination: str, price: int, is_add=True):

        if self.exists_route(origin, destination):
            return {'msg': 'route exists.'}

        route = [[origin, destination, price]]
        self._graph.insert_point_and_destination(route, is_add)

        resp = self.write_in_file(destination, origin, price)

        return resp

    def write_in_file(self, destination: str, origin: str, price: int):
        resp = {}
        if self.file.writer_file(origin, destination, price):
            resp = dict(origin=origin, destination=destination, price=int(price))
        return resp

    def get_route(self, origin: str, destination: str):
        return self._graph.find_best_price(origin, destination)

    def exists_route(self, origin: str, destination: str):
        return self.file.exists_route(origin, destination)

    def exists_point(self, origin: str, destination) -> bool:
        exists_origin = self._graph.exists_point(origin)
        exists_destination = self._graph.exists_point(destination)

        return exists_origin and exists_destination
