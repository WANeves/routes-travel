import csv
import sys
from typing import TextIO


class File(object):

    def __init__(self, input_file=None):
        self._input_file = input_file
        self._graph = {}
        self._input_data = []

    @property
    def input_data(self) -> list:
        return self._input_data

    def __open_file(self, file, open_mode) -> TextIO:
        return open(file, open_mode)

    def __read_file(self):

        data_list = []

        with self.__open_file(self._input_file, "r") as file:
            reader = csv.reader(file)
            try:
                for line in reader:
                    data_list.append(line)
            except csv.Error as e:
                sys.exit('file %s, line %d: %s' % (self._input_file, reader.line_num, e))

        file.close()
        self._input_data = data_list

    def read_entry(self):
        self.__read_file()

    def exists_route(self, origin: str, destination: str) -> bool:
        self.read_entry() # reload input_data
        for item in self._input_data:
            if item[0] == origin and item[1] == destination:
                return True
        return False

    def writer_file(self, origin: str, destination: str, price: int) -> bool:
        try:
            new_route = origin + ',' + destination + ',' + str(price)
            with open(self._input_file, 'a+') as file:
                file.write(new_route)
                file.write('\n')
                file.close()

            return True
        except:
            return False

    def clear_file(self):
        file = open(self._input_file, "r+")
        file.truncate(0)
        file.close()
